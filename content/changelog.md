---
title: "Changelog"
date: 2024-12-02T00:00:00
draft: false
---

<br/>

## Cwtch 1.15.4 - December 2nd 2024

This is an incremental release that fixes a crash bug on older Android devices, provides a few upgrades to dependencies, and introduces tighter restrictions on unidentified contracts using the newer Enhanced Permissions model.

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **Improvements:**
    - Fix android gating of new API features which was previously causing Cwtch to crash on older versions of Andorid
    - Ugrades bundles tor to 0.4.8.13
    - Chat history is no longer fetched all at once and is now buffered, greatly improving the scrolling performance when viewing long message threads Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/944)
    - Search context is now correctly unloaded when the back button is pressed. Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/947)
    - When downloading shared files, Cwtch no defaults to the users Downloads directory (this can be changed in Settings)
    - New contacts now default to more restricted permissions and no longer have the ability to request name or profile attributes
    - Unaccepted contacts will no longer be automatically considered for reconnection
    - Fixes to Themeing:
        - Fixes for importing custom themes and assets
        - Fix theme coloring of the appbar, panes and other various widgets
        - Profile pictures now show offline status in the Messaging Pane

### Reproducible Bindings

Cwtch 1.15.4 is based on libCwtch version `libCwtch-autobindings-2024-11-29-14-38-v0.1.5`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.5](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.5)
<hr/>
<br/>

## Cwtch 1.15.0 - October 8th 2024

{{% release file="/static/releases/cwtch-v1.15/changelog.md" %}}


## Cwtch 1.14.7 - Februrary 27th 2024

{{% release file="/static/releases/cwtch-v1.14.7/changelog.md" %}}


## Cwtch 1.14 - Februrary 14th 2024

{{% release file="/static/releases/cwtch-v1.14/changelog.md" %}}

## Cwtch 1.13.2 - November 30th 2023

{{% release file="/static/releases/cwtch-v1.13.2/changelog.md" %}}

## Cwtch 1.13.1 - October 3rd 2023

{{% release file="/static/releases/cwtch-v1.13.1/changelog.md" %}}

## Cwtch 1.13 - September 27th 2023

{{% release file="/static/releases/cwtch-v1.13/changelog.md" %}}


## Cwtch Beta 1.12 - June 16th 2023

{{% release file="/static/releases/cwtch-v1.12/changelog.md" %}}

## Cwtch Beta 1.11 - March 29th 2023

{{% release file="/static/releases/cwtch-v1.11/changelog.md" %}}

## Cwtch Beta 1.10 - December 16th 2022

{{% release file="/static/releases/cwtch-v1.10/changelog.md" %}}

## Cwtch Beta 1.9 - September 10th 2022

{{% release file="/static/releases/cwtch-v1.9/changelog.md" %}}


## Cwtch Beta 1.8 - June 28th 2022

{{% release file="/static/releases/cwtch-v1.8/changelog.md" %}}


## Cwtch Beta 1.7.1 - May 2nd 2022

{{% release file="/static/releases/cwtch-v1.7.1/changelog.md" %}}

## Cwtch Beta 1.7 - April 22nd 2022

{{% release file="/static/releases/cwtch-v1.7/changelog.md" %}}

## Cwtch Beta 1.6 - February 11th 2022

{{% release file="/static/releases/cwtch-v1.6.2/changelog.md" %}}

# Cwtch Beta 1.5 - December 21st 2021

For a more detailed overview of changes present in the 1.5 release see issues tagged [cwtch-beta-1.5](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=157&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Inline Image Preview Experiment**
          - New experiment allows image files to be auto download and displayed
    - **Profile level server management**
        - See what servers your profile is connecting to, label them, and see what groups are hosted on them
        - Import hosted servers easily from the UI now
        - When creating groups, see the target server's description in the dropdown
    - **Clickable Hyperlinks Experiment**
        - Thanks to [Nima Boscarino](https://www.n11o.com/) 
    - Many new themes! Ghost, Mermaid, Midnight, Neon 1 & 2, Pumpkin, Vampire, and Witch all with Dark and Light modes
* **Bug Fixes  / Improvements:**
    - New Storage System
        - On the first load of any profile the Cwtch splash screen will show with a progress spinner while the old profile is migrated to the new storage system. This is a one time process, and may take up to a minute
        - This will enable a lot of improvements and new features going forward, and should be both more scalable and faster
    - Hosted server metrics: We now display "total message count" and "connections" on the server's pane
    - Big improvements to message pane rendering speed on Android thanks to message caching enabled by new storage system
    - Fixed crash on startup if hosting servers that were not marked "autostart"
    - Minor improvement to server connection speed on startup
    - Windows Uninstaller
* **Accessibility / UX:**
    - Full French, Russian and Italian Translations!
        - Splash screen text is now translatable
    - Tor info pane displays bootup progress when starting
    - Adding contact pane now slides up on Android when keyboard is activated on small screens
    - Minor message padding adjustments for Android to be more efficent with limited display width

<hr/>

## Cwtch Beta 1.4 - November 5th 2021

For a more detailed overview of changes present in the 1.4 release see issues tagged [cwtch-beta-1.4](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=156&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
  - Server Hosting Experiment! Please help us test this.
      - Go to Settings to Enable the Server Hosting Experiment
      - Create your own Servers to host groups on, share these publicly to allow others to privately host
        groups on your infrastructure or keep them private for more efficient groups
      - Only supported on Desktop applications
  - File Sharing Experiment now supports resumption within a 30 day window
      - Note: 1.4 File Sharing is *not* compatible with previous versions
* **Bug Fixes  / Improvements:**
  - Improved handling of errored group messages in the UI
  - Consolidated timeline code for P2P and Group conversations
      - Together these should fix ordering instabilities seen in group chats
  - Introduction of new Zoned Attributes
  - Mac OS builds are now automatically built as part of our release pipeline - which means nightly releases are now available!
* **Accessibility / UX:**
  - Up to date French Translation
  - The start of a Polish Translation
* **Planning / Design**
  - We published [a new roadmap](https://openprivacy.ca/discreet-log/19-cwtch-roadmap/) for the next few versions of Cwtch Beta
  - Storage Engine Redesign (Implementation underway for 1.5)
  - Initial discussions/ draft of the new Hybrid Groups Protocol (planned introduction in 1.6)    
  - Design of Additional UI Themes (to be included in 1.5)
  
<hr/>

## Cwtch Beta 1.3 - October 1st 2021

For a more detailed overview of changes present in the 1.3 release see issues tagged [cwtch-beta-1.3](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=154&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the volunteer translators and testers who made this release possible.

* **New Features:**
  - File Sharing Experiment! Please help us test this.
     - Go to Settings to Enable the File Sharing Experiment
     - Share files with both Contacts and Groups without uploading to a centralized file server
     - For more information see our most recent [Discreet Log #17: Filesharing FAQ](https://openprivacy.ca/discreet-log/17-filesharing-faq/)
  - Streamer/Redaction Mode - Hide sensitive data (addresses etc.) when showing Cwtch to others
      - Enable in Settings
* **Bug Fixes  / Improvements:**
   - Light Mode Fixes
   - Fix Crash that sometimes occurred after Deleting a Group
   - Reply buttons on Android now auto-hide
   - Shutdown button on Mac now works
   - Invitations on Android now persist state
   - Linux / Mac packaging / installer Improvements
   - **(Tapir)** More robust cryptographic checking on Cwtch addresses (these would have
       previously been caught in higher level authentication protocols, but are now rejected when importing
       addresses)
   -  **(Tapir)** Duplicate connections are now explicitly resolved down to a single connection. This should
    improve performance and latency, especially in mobile environments
* **Accessibility / UX:**
    - Completed French Translation
  
<hr/>

## Cwtch Beta 1.2 - August 31st 2021

For a more detailed overview of changes present in the 1.2 release see issues tagged [cwtch-beta-1.2](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=152&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the volunteer translators and testers who made this release possible.

* **New Features:**
  - Mac OS Version! Please help us test this.
  - An icon is now displayed when blocking of unknown contacts is enabled. 
  - Chat now maintains your last read position and provides a quick way of scrolling to most recent messages.
  - The `IME_FLAG_NO_PERSONALIZED_LEARNING` flag on all text fields (private keyboard mode)
  - (Cwtch Server) New Docker Container
  - Archive Conversations
* **Bug Fixes  / Improvements:**
  - Restyling of Message Composition bar buttons
  - Double Pane Scrolling Fixes
  - Fix Keyboard Issues on Android
  - Fix Cursor Issues on Android
  - Message Rows now have draggable/animated feedback for gesture-based actions  
  - Upgrade to Flutter 2.5
  - Memory Use Improvements
  - (Group Experiment) Previously, group signatures assumed that `Group ID` was kept secret from the server in order to preserve
    metadata privacy. This is no longer assumed 1.2 which now only relies on `Group Key` being kept secret. This change is backwards incompatible and as such 1.1 clients will not be able to verify group messages from newer clients. For more
    information please consult the [cwtch secure development handbook](https://docs.openprivacy.ca/cwtch-security-handbook/cwtch-overview.html#multi-party-conversations-groups-and-peer-to-server-communication)
* **Accessibility / UX:**
  - Completed French Translation
  - References to `Peer` replaced with more specific language (e.g. `Contact` or `Conversation`) throughout the UI

<hr/>

## Cwtch Beta 1.1  - July 15th 2021

For a more detailed overview of changes present in the 1.1 release see issues tagged [cwtch-beta-1.1](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=closed&labels=151&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the volunteer translators and testers who made this release possible.

* **New Features:**
  - Support for Quoted Messages / Reply-to
  - Support for Multi-line Messages
  - Enable Autofill for Password Fields on Android
  - New Windows Installer
  - Improved Linux Install Scripts
* **Bug Fixes  / Improvements:**
  - Client-side filtering of messages from blocked contacts when in group conversations
  - Improved handling of syncing groups on Android
  - Prevent sending empty messages
  - Prevent sending messages to offline contacts / servers
  - Fix high CPU usage on Windows
  - Fix for unintentional shutdown prompt when clicking on a notification in Android
  - Fix non-truncating contact names on Android
  - Fix regression that preventing unblocking contacts (*v1.1.1*)
* **Accessibility / UX:**
  - Completed French Translation
  - Updated German and Italian Translations (now 85% complete)
  - Improved Window Icon  
  - Improved Group Syncing UI   
  - Contextual explanations for encrypted/unencrypted profiles
  - Tooltips for various UI elements
  
<br/>

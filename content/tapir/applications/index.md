---
---

<meta name="go-import" content="cwtch.im/tapir git https://git.openprivacy.ca/cwtch.im/tapir">
<meta name="go-source" content="cwtch.im/tapir https://git.openprivacy.ca/cwtch.im/tapir https://git.openprivacy.ca/cwtch.im/tapir/src/master{/dir} https://git.openprivacy.ca/cwtch.im/tapir/raw/master{/dir}/{file}#L{line}">

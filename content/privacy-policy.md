---
title: "Privacy Policy"
date: 2020-12-10T14:07:19-08:00
draft: false
---



Cwtch is a surveillance-resistant application. Cwtch has been designed to be metadata-resistant, that is, it does not leak information about who you are, what you are doing, or who you are communicating with.

Cwtch is developed by the Open Privacy Research Society (Open Privacy for short), a non-profit organization, legally incorporated in British Columbia, Canada.
                                                           
Open Privacy is strongly committed to protecting your privacy online. We do not, and in many cases cannot, collect information about who uses Cwtch or how Cwtch is used, subject to the following exceptions:

### Bug Reports and Feedback

If you choose to send feedback regarding Cwtch via the app, our issue tracker, an app store or other medium, we may store your feedback in order to fix any issues described.

Note that if you choose to share an identifier (like an email address, or a cwtch public key) we may use that identifier to contact you regarding your report or feedback.

### Downloads, Usage Statistics and Website Operations

We do not log unique information (e.g. IP Addresses) regarding visits to this website, including downloads of Cwtch.

We do log and store aggregate website access logs for the purposes of website maintenance, and related operations. These logs do not contain any identifiable information.

If you download Cwtch via a third party appstore, e.g. Google Play, information about that download is governed by the privacy policy of that third party application.

## Contact

If you have any questions about this privacy statement, your personal information, or the practices of this Site, 
you can write to [contact@openprivacy.ca](mailto:contact@openprivacy.ca).
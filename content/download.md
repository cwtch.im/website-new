---
title: "Download Cwtch"
date: 2022-04-22T00:00:00
draft: false
---

<br/><br/>

<div class="row text-center align-content-center">
Cwtch is now available for the following platforms:
</div>

<br/><br/>
<div class="row text-center align-content-center">
<div class="col-1"> </div>
<a class="col-2 text-center align-content-center" href="#android"><img class="downloadicon" src="/assets/android.svg"/></a>
<a class="col-2 text-center align-content-center" href="#windows"><img class="downloadicon" src="/assets/windows.svg"/></a>
<a class="col-2 text-center align-content-center" href="#linux"><img class="downloadicon" src="/assets/linux.svg"/></a>
<a class="col-2 text-center align-content-center " href="#mac"><img class="downloadicon" src="/assets/apple.svg"/></a>
<a class="col-2 text-center align-content-center" href="#source"><img class="downloadiconnoinvert" src="/assets/OP_eye.svg"/></a>
</div>

<hr/>
<br/><br/>
<br/>

<h3 id="android"><img class="downloadicon" src="/assets/android.svg"/> Android</h3>

* Standalone APK (works for arm7 and arm64):  [cwtch-v1.14.7.apk](https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.14.7/cwtch-v1.14.7.apk)
    * `sha512: 167b697203cbdcb666213d08caeaf0d323e029f99732c6d089a80d3458daf6dd45f599b4ca92d570a1a29196790871ceb32f4042068f0a26cc1014a138f538b1`
    * Note: Syncing experimental Groups on some Android devices can be very slow depending on the underlying hardware and configuration.
* <a class="col-2 text-center align-content-center" href="https://play.google.com/store/apps/details?id=im.cwtch.flwtch"><img class="downloadicon2" src="/images/google-play-badge.png"/></a>

<br/>

<h3 id="windows"><img class="downloadicon"  src="/assets/windows.svg"/>  Windows</h3>


* Installer: [cwtch-installer-v1.14.7.exe](https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.14.7/cwtch-installer-v1.14.7.exe)
   * `sha512: 4ef7b98c7b7ca9f3453b1fc0503f0564b3819545ad5cfb54c556dbb220d8c36f60833cfeee3c8769f9d42c32827cf7c77b1c85c99acc6c1417f093bcfcba839e`
* EXE: [cwtch-v1.14.7.zip](https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.14.7/cwtch-v1.14.7.zip)
    * `sha512: 612d1fc9a2e2e48f150cc464f265447ea0cbb45344fc4787344b1665e9befd49f99c0dbf5e19bcc3bba0d5d6df92085d14bd212270b2148b60a1d4ec9aef1c41`
    * Due to the newness of the Open Privacy certificate, Windows Defender Smart Screen may throw up a warning the first time you try to run the app. To bypass, simply click the "More info" button and then "Run Anyways"


<br/>

<h3 id="linux"><img class="downloadicon"  src="/assets/linux.svg"/>  Linux</h3>

* amd64: [cwtch-v1.14.7.tar.gz](https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.14.7/cwtch-v1.14.7.tar.gz)
    * `sha512: 465e741781bf342c0bb810dd2f67145628336c05f9e57390045d47d7f3e24be2bf3bc882b6595aade24d7668e46c713ce1b9b66d4b61dac32b80a0dc1c1e9e84`


<br/>

<h3 id="mac"><img class="downloadicon"  src="/assets/apple.svg"/>  MacOS</h3>


* DMG: [Cwtch-v1.14.7.dmg](https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.14.7/Cwtch-v1.14.7.dmg) (works for x86_64 and arm64e)
  * `sha512: f81808c1f02824e4cfbf6758e286bfb0bcd73ff6b865ebfd8f1c2944655b3eac64cbbe80f5b27124167ad3b22098bf1e9a7d747f0992fc57e5f63e87b21cff3e`


<br/>

<h3><img class="downloadicon"  src="/assets/apple.svg"/>  iOS</h3>

* We would love to offer an iOS however there are several limitations of the platform that make this difficult. If you would like to support this effort please consider [volunteering](https://cwtch.im/#how-to-support) or [donating](https://www.patreon.com/openprivacy).

<br/>

<h3 id="source"><img class="downloadiconnoinvert" src="/assets/OP_eye.svg"/>  Build from Source</h3>

Everything we build is open source:

* libcwtch-go: [https://git.openprivacy.ca/cwtch.im/libcwtch-go](https://git.openprivacy.ca/cwtch.im/libcwtch-go)
* Cwtch UI:  [https://git.openprivacy.ca/cwtch.im/cwtch-ui](https://git.openprivacy.ca/cwtch.im/cwtch-ui)

<br/><br/>

---
title: "Cwtch 1.15 Release"
date: 2024-10-18T00:00:00
draft: false
altlink: "https://cwtch.im/download"
---

## Cwtch 1.15 is now available for [download](/download/)

{{% release file="/static/releases/cwtch-v1.15/changelog.md" %}}

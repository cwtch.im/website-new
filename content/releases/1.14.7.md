---
title: "Cwtch 1.14 Release"
date: 2024-02-14T00:00:00
draft: false
altlink: "https://cwtch.im/download"
---

## Cwtch 1.14 is now available for [download](/download/)

{{% release file="/static/releases/cwtch-v1.14/changelog.md" %}}

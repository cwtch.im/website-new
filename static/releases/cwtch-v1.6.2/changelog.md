For a more detailed overview of changes present in the 1.6 release see issues tagged [cwtch-beta-1.6](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=158&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Custom Profile Images**
        - This requires enabling the Filesharing and Image Preview experiments
    - **Advanced Tor Configuration**
        - Cwtch can now be configured to cache Tor consensus information, use predefined ports, and connect to an external, system Tor
        - Tor Circuit information is now available for P2P Conversations
    - **Notification Policies**
        - You can now mute specific conversations, turn off notifications entirely,
          or switch all notifications to opt-in
* **Bug Fixes  / Improvements:**
    - Images are now displayed for the sender as well as the recipients
    - Deleting P2P contacts is now supported again
    - *Clickable Links* will no longer prepend `https` to the text of the
      displayed URL
    - Closing via the windows exit button on Linux now triggers Cwtch to
      shut down cleanly
    - Packaged Tor has been upgraded
    - New [integration testing infrastructure](https://openprivacy.ca/discreet-log/23-cucumber-testing/)
    - Fixes to prevent extensive word wrapping on Android Devices (1.6.1)
    - Num-pad Enter now triggers sending a message, instead of inserting a new line (1.6.1)
    - Sending a message via a keyboard or via the send button now invokes identical logic (1.6.1)
    - Android Notification Image Fetching fixes (1.6.2)
    - Android Group Sync Status fixes (1.6.2)
    - Message View Rendering Issues fixes (1.6.2)
    - Improved Android Worker Robustness on Restart/Shutdown (1.6.2)
* **Accessibility / UX:**
    - Notifications are now translatable
    - Translations: French, Italian, German, Spanish, Romanian, Norwegian, Danish, Welsh, Russian, Polish, Luxembourgish and Greek (1.6.2)
    - Settings Pane is now broken down by subheadings
    - Message view now displays an estimated character count/limit for new messages
    - Fix bug that allows multiple file dialog windows to be opened when sharing
      a file
    - Number of unread messages from other loaded profiles are now summarized on the active profile

<hr/>
<br/>
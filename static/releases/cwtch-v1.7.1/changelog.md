* **Bug Fixes  / Improvements:**
    - File download bubble fixes for when file is moved
    - Fixes for android reply to message
    - Fixes for message cache loading especially on android
* **Accessibility / UX:**
    - Minor improvements to message pane rendering on loading messages
    - Improve "new messages" bubble behaviour

<hr/>
<br/>

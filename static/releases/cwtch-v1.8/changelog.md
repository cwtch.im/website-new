For a more detailed overview of changes present in the 1.8 release see issues tagged [cwtch-beta-1.8](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=160&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **New Message Formatting Toolbar** 
    - **Apple Silicon Support**
    - **Brand New Documentation Handbook**: [docs.cwtch.im](https://docs.cwtch.im)
* **Bug Fixes  / Improvements:**
    - Clicking on a Quoted Message will not scroll to that message
    - Quoted messages are now clipped to single line to maximize space
    - Clicking a contact in a conversation to initiate a DM will now scroll the conversation list to that contact.  
    - Cwtch is now based on Flutter 3
       - Linux: Holding down a keyboard key will now repeat input
    - Dropdown fields in Settings are now correctly scaled
    - Bug Fix preventing archived messages from displaying their correct timestamp
    - Syncing a new group on a new server will now update the syncing progress bar
    - `Address` has been removed from Group Conversation Settings as it no long has any practical use
* **Accessibility / UX:**
    - Full translations for **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, and **Welsh**
    - Core translations for **Danish** (89%), **Norwegian** (89%), and **Romanian** (89%)
    - Partial translations for **Luxembourgish** (24%), **Greek** (19%), and **Portuguese** (7%)
    - Add Contact UX has been split to prompt for a specific action instead on directly opening the Add Contact pane
    - Several small updates to theming
    - Image / File display overlay now displays timestamp like other messages
    - Snackbar notifications have been added for all copy actions
    - Conversation row now displays the date instead of time for conversations between 1 and 2 days old
    - Conversation row is now split onto multiple lines to better use space
    - When scrolling up a conversation, a button is now displayed to allow quickly scrolling to the most recent messages

<hr/>
<br/>
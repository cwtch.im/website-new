A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
    - **Based on new Reproducible Cwtch Stable Autobuilds** - this is the first release of cwtch based on [reproducible Cwtch bindings](https://docs.cwtch.im/blog/cwtch-bindings-reproducible) in addition to our new [automatically generated](https://docs.cwtch.im/blog/autobindings)
    - **Two New Supported Localizations**: **Slovak** and **Korean**
- **Bug Fixes  / Improvements:**
    - When preserving a message draft, quoted messages are now also saved
    - Layout issues caused by pathological unicode are now prevented
    - Improved performance of message row rendering
    - Clickable Links: Links in replies are now selectable
    - Clickable Links: Fixed error when highlighting certain URIs
    - File Downloading: Fixes for file downloading and exporting on 32bit Android devices
    - Server Hosting: Fixes for several layout issues
    - Build pipeline now runs automated UI tests
    - Fix issues caused by scrollbar controller overriding
    - Initial support for the Blodeuwedd Assistant (currently compile-time disabled)
    - Cwtch Library:
        - [New Stable Cwtch Peer API](https://docs.cwtch.im/blog/cwtch-stable-api-design/)
        - Ported File Downloading and Image Previews experiments into Cwtch
- **Accessibility / UX:**
    - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, **Turkish**, and **Welsh**
    - Core translations for **Danish** (75%), **Norwegian** (76%), and  **Romanian** (75%)
    - Partial translations for **Luxembourgish** (22%), **Greek** (16%), and **Portuguese** (6%)


### Reproducible Bindings

Cwtch 1.11 is based on libCwtch version `2023-03-16-15-07-v0.0.3-1-g50c853a`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.3-1-g50c853a](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.3-1-g50c853a)

<hr/>
<br/>

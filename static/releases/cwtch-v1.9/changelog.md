For a more detailed overview of changes present in the 1.9 release see issues tagged [cwtch-beta-1.9](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=160&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **View Replies** - quickly view all replies to a specific message 
    - **Manage Shared Files** - pause or restart sharing of files to specific conversations
    - **Pin Conversations** - important conversations can now be pinned to the top of the conversations list
    - **Experiment: QR Codes** - start of functionality allowing sharing of Cwtch addresses via QR codes
    - **Cwtch Handook** is now available in [Italian](https://docs.cwtch.im/it/docs/intro/), [German](https://docs.cwtch.im/de/docs/intro/) and [Spanish](https://docs.cwtch.im/es/docs/intro/)
* **Bug Fixes  / Improvements:**
    - Group messages are now viewable while the group is actively syncing 
    - Group Anti-spam challenge/completion status is now surfaced in the UX 
    - Fixed duplicate contact add bug (Android)
    - Introduced better error handling for the file sharing experiment
      - Automatic downloads are now not triggered if download directory does not exist or is incorrectly configured
      - Failed file downloads can now be restarted through the UX
    - Tor Version is now surfaced correctly in UI after restarts
    - Upgrade bundled Tor
    - Custom Tor SOCKS port configuration is now used
    - Peering attempts are now paused until Tor is fully bootstrapped
    - Per-conversation messages drafts are now saved while Cwtch is open
* **Accessibility / UX:**
    - Full translations for **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, **Turkish**, and **Welsh**
    - Core translations for **Danish** (85%), **Norwegian** (85%), and  **Romanian** (85%)
    - Partial translations for **Luxembourgish** (25%), **Greek** (19%), **Dutch** (12%), and **Portuguese** (7%)
    - Updates to experiment descriptions to remove outdated references to previous versions

<hr/>
<br/>
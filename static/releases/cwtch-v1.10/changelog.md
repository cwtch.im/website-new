For a more detailed overview of changes present in the 1.10 release see issues tagged [cwtch-beta-1.10](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=169&milestone=0&assignee=0) in our issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Fine-grained Profile Autostart** - you can now control which profiles are automatically enabled at start up
    - **New Connection Backend** - we have reworked connection management in Cwtch to minimize contention inside the Tor process, and to prioritize regular contacts. This results in faster peering times, and more stable connections 
* **Bug Fixes  / Improvements:**
    - [Profile Exporting](https://docs.cwtch.im/docs/profiles/exporting-profile) now works on Android devices
    - [Profile Image Shares](https://docs.cwtch.im/docs/profiles/change-profile-image) are now re-initialized every start up to ensure that profile images are available to new contacts. (Previously profile image shares expired after 30-days like all other file shares)
    - Fix a bug that prevented sharing files on Android in certain configurations
    - Many [colorscheme improvements](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/590) to packaged themes
    - A new **Juniper** theme
    - Improved UX for [unlocking profiles](https://docs.cwtch.im/docs/profiles/unlock-profile)
    - Fix a bug that prevented the [deletion of profiles](https://docs.cwtch.im/docs/profiles/delete-profile)
    - [Streamer mode](https://docs.cwtch.im/docs/settings/appearance/streamer-mode) now hides cwtch addresses on configuration screens
    - Message formatting now applies to quoted messages
    - Message formatting can now be disabled when experiments are disabled
    - Fix bug that prevented Cwtch from finding packaged Tor binary in some installations
    - Upgrade to Flutter 3.3.5
    - New Android Splash Screen
* **Accessibility / UX:**
    - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, **Turkish**, and **Welsh**
    - Core translations for **Danish** (85%), **Norwegian** (85%), and  **Romanian** (85%)
    - Partial translations for **Luxembourgish** (25%), **Greek** (19%), and **Portuguese** (7%)

<hr/>
<br/>

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Managed Groups Alpha** - This Cwtch release contains the first iteration of managed groups, allowing a dedicated (and trusted) cwtch peer to host a multi-person chat. To volunteer to test out this new feature please [join the Release Candidate Testers group](https://docs.cwtch.im/docs/contribute/testing#join-the-cwtch-release-candidate-testers-group) and ask for an invite.
  - **Private Profile Labels** - You can now give each of your profiles a local-only descriptive name.
- **Bug Fixes  / Improvements:**
  - Cwtch is now based on Flutter 3.22
  - Cwtch on Linux now uses the Flutter-provided `fl_dart_project_set_aot_library_path` method (contributed by Dan), rather than our own custom solution.
  - File Download verification attempts with incomplete/missing manifests will now allow attempt to redownload the manifest rather than emitting an error.
  - Cwtch will now correctly connect to the Tor Service in Tails OS > 6.0
  - Importing Unencrypted Profiles no longer requires entering [the defecto password](https://docs.cwtch.im/security/components/cwtch/profile-encryption#unencrypted-profiles-and-the-default-password)
  - When the clickable links experiment is enabled, message Formatting now priotizes code formatting over linking. Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/923)
  - When the image previews experiment is enabled, images are now previewed in their original aspect ratio. Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/929)
  - Source Tarballs are now also packaged during the release process
  - The Windows installer is now signed
  - New deb package release option (beta, please help us test!)
  - New *Synergy* Theme, for those who want Cwtch to look like an enterprise product
- **Accessibility / UX:**
  - Core translations for **Brazilian Portuguese**, **Danish** , **Dutch**, **French**, **German**, **Italian**, **Norwegian** , **Romanian** , **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Partial translations for **Korean** (41%), **Japanese** (26%), , **Luxembourgish** (19%), **Greek** (15%), **Uzbek** (9%), and **Portuguese** (5%)
  - **Theme Refresh** - Many small adjustments to existing themes to make them more accessible

    
### Reproducible Bindings

Cwtch 1.15 is based on libCwtch version `libCwtch-autobindings-2024-09-05-11-07-v0.1.3`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.3](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.3)
<hr/>
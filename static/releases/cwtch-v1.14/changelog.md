A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Custom Themes** - You can now load [custom themes](https://docs.cwtch.im/docs/contribute/themes) into Cwtch.
  - **Message View Backgrounds** - This release contains the first support for (optional) background images in the message view. Future releases will allow per-conversation images.
- **Bug Fixes  / Improvements:**
  - Fixed tor connectivity in newer Tails releases
  - Fixes in the Retry Plugin for better managing of a large number of contacts
  - Several UX improvements for font scaling, and styling
  - Fixed Android File Sharing Bug which prevent downloads of [non-previewed files](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures).
  - Fixed Android File Sharing Bug that resulted in a UI reset triggered by a rate race condition between reconnection and new message arriving
  - Split Settings Pane into multiple tabs for easier navigation of options
  - Fixed contact row date time/localization inconsistency
  - Fixed contact row issue where `LANG` wasn't set on some linux systems
  - libCwtch now support older Mac releases (min 10.12)
  - Updates images and descriptions in the Windows Installer
- **Accessibility / UX:**
  - Core translations for **Brazilian Portuguese**, **Danish** , **Dutch**, **French**, **German**, **Italian**, **Norwegian** , **Romanian** , **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Partial translations for **Korean** (41%), **Japanese** (26%), , **Luxembourgish** (19%), **Greek** (15%), **Uzbek** (9%), and **Portuguese** (5%)
  - **Theme Refresh** - Many small adjustments to existing themes to make them more accessible
  
## Reproducible Bindings

Cwtch 1.14 is based on libCwtch version `libCwtch-autobindings-2024-02-12-11-04-v0.0.12`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.12](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.12)
<hr/>
<br/>

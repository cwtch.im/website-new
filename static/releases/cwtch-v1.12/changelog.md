A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Profile Attributes** - profiles can now be augmented with [additional public information](https://docs.cwtch.im/docs/profiles/profile-info)
  - **Availability Status** - you can now notify contacts that you [are **away** or **busy**](https://docs.cwtch.im/docs/profiles/availability-status)
  - **Five New Supported Localizations**: **Japanese**, **Korean**, **Slovak**, **Swahili** and **Swedish**
  - **Support for Tails** - adds an [OnionGrater](https://docs.cwtch.im/docs/platforms/tails) configuration and a new `CWTCH_TAILS` environment variable that enables special Tor behaviour.
- **Bug Fixes  / Improvements:**
  - Based on Flutter 3.10
  - Inter is now the main UI font
  - New Font Scaling setting
  - New Network Management code to better manage Tor on unstable networks
  - File Sharing Experiment Fixes
  	- Fix performance issues for file bubble
  	- Allow restarting of file shares that have timed out
  	- Fix NPE in FileBubble caused by deleting the underlying file
  	- Move from RetVal to UpdateConversationAttributes to minimze UI thread issues
  - Updates to Linux install scripts to support more distributions
  - Add a Retry Peer connection to prioritize connection attempts for certain conversations
  - Updates to `_FlDartProject` to allow custom setting of Flutter asset paths
- **Accessibility / UX:**
  - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Core translations for **Danish** (75%), **Norwegian** (76%), and  **Romanian** (75%)
  - Partial translations for **Japanese** (29%), **Korean** (23%), **Luxembourgish** (22%), **Greek** (16%), and **Portuguese** (6%)

## Reproducible Bindings

Cwtch 1.12 is based on libCwtch version `libCwtch-autobindings-2023-06-13-10-50-v0.0.5`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.5](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.5)

<hr/>
<br/>

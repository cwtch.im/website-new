A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Conversation Search** - Cwtch can now find messages based on their content.
  - **Appear Offline Mode** - in this mode Cwtch does not launch a listening service for inbound contacts, and allows a profile to be more selective in the contacts they connect to.
  - **Whonix Support** - new runtime flags make changes that allow Cwtch to [run on Whonix](https://docs.cwtch.im/docs/platforms/whonix)
  - **Save History Global Setting** - by default Cwtch deletes all messages on shutdown unless a conversation is otherwise configured. This change allows a user to change this default behaviour.
- **Bug Fixes  / Improvements:**
  - Based on Flutter 3.13.4
  - Updated Android Target to 33
  - Profile Status Menu now has many more options, including offline status, edit profile and enabling/disabling profile
  - File Sharing Bug Fixes
    - Manage shared files now supports re-enabling older file shares
  - Improvements towards [UI Reproducible Builds](https://docs.cwtch.im/blog/cwtch-ui-reproducible-builds-linux)
  - Server Info now propagates to the UI consistently
  - Prevent DBus Exceptions on platforms where it is unsupported
  - Packaged Emoji Font
  - Fixes to retry manager which have greater improved (re)connection efficacy
  - Allow deleting server info in Manage Servers
- **Accessibility / UX:**
  - Core translations for **Brazilian Portuguese**, **Danish** , **Dutch**, **French**, **German**, **Italian**, **Norwegian** , **Romanian** , **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Partial translations for **Korean** (37%), **Japanese** (27%), , **Luxembourgish** (20%), **Greek** (15%), **Uzbek** (10%), and **Portuguese** (5%)
  - Font Scaling improvements on several screens
  
## Reproducible Bindings

Cwtch 1.13 is based on libCwtch version `libCwtch-autobindings-2023-09-26-13-15-v0.0.10`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10)
<hr/>
<br/>

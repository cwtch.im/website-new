For a more detailed overview of changes present in the 1.7 release see issues tagged [cwtch-beta-1.7](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=159&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Profile Export / Import** - You can now move your profiles across devices by exporting them from one
      client and importing them in another. Note that there is still the limitation that only one instance of a profile
      can run at any one time
    - **New Message Formatting Experiment** - We have introduced a new experiment that allows a subset of markdown
      in messages including **bold**, *italic* and `code` - please see [Disceet Log 28](https://openprivacy.ca/discreet-log/28-message-text-formatting/)
      for more information
    - **Android Stability Improvements** - One of the biggest chunks of work represented by this release is several
  improvements to the Android version of Cwtch. You can find our more in [Disceet Log 27](https://openprivacy.ca/discreet-log/27-android-improvements/)
      for more information
    - **New Setting: Android Battery Exemption Request** - you can now request a power management exemption for the Cwtch application
      on Android directly from the Settings pane. Enabling this may allow Cwtch to run better on Android at the cost
      of increased battery use
* **Bug Fixes  / Improvements:**
    - Android Security Flags - We have enabled several `FLAG_SECURE` on Android
    - Folder Selection Bug Fix
    - UI Profiles Pane now integrates output from the Network Health plugin to display if a profile appears offline
    - Improvements to the UI Message Cache including a new Bulk Fetching/Loading API for improved performance
    - Several small thread and memory inefficiencies and leaks have been fixed in the UI and underlying libraries
      resulting in substantially reduced memory use
    - Notification Policy Setting bug fix
    - Unread messages counts are now saved between client restarts on Android
    - In development mode the Settings pane fetches and displays output from `GetDebugInfo`
    - `CWTCH-PROFILE` environment variable will cause Cwtch to output profiling information when displaying Debug Info
    - Fixes and updates to the (un)installer for Cwtch on Windows 
* **Accessibility / UX:**
    - Full translations for new features for French, German and Italian.
    - Add Contact UX has been split to prompt for a specific action instead on directly opening the Add Contact pane
    - Several small updates to theming
    - Image / File display overlay now displays timestamp like other messages

<hr/>
<br/>
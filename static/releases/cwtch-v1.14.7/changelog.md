A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

This fix mainly addresses several long standing issues with Android, but also contains an important update to libCwtch.

- New Android 13 Foreground Service Permission (`FOREGROUND_SERVICE_DATA_SYNC`) - to ensure better stability
- Fixes an issue that prevented some contacts from fetching profile images
- Android Notification fix to prevent exceptions when displaying full "Conversation Info" notifications with profile images enabled
- Cwtch Reload functionality fixed on Android to prevent over-zealous deletion of messages for contacts with unsaved history
- A few minor UI fixes for Android and Desktop to better fit small screens (more on the way!)

## Reproducible Bindings

Cwtch 1.14.7 is based on libCwtch version `libCwtch-autobindings-2024-02-26-18-01-v0.0.14`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.14

<hr/>
<br/>

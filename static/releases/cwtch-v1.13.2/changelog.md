- Upgrade Packaged Tor to 0.4.8

## Reproducible Bindings

Cwtch 1.13.2 is based on libCwtch version `libCwtch-autobindings-2023-09-26-13-15-v0.0.10`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10)
<hr/>
<br/>
